" arg-paste
function! ArgPasteModifyRegister(before)
    let l:data = getreg(v:register)
    let l:line = getline('.')
    let l:col = col('.')
    let l:char = l:line[l:col-a:before-1]
    let l:nextchar = l:line[l:col-a:before]

    if l:char =~? '(\|\[' && l:data[0] == ','
        let l:data = substitute(l:data, ',\(\s*\)\(.*\)', '\2,\1', '')
    elseif l:nextchar =~? ')\|]' && l:data =~? ',\s*$'
        let l:data = substitute(l:data, '\(.*\),\(\s*\)', ',\2\1', '')
    elseif l:char == ',' && l:data[0] == ','
        let l:data = substitute(l:data, ',\(.*\)', '\1,', '')
    endif

    call setreg(v:register, l:data)
endfunction

function! MapArgDefault(name, mode)
    let l:mapping = maparg(a:name, a:mode)
    if empty(l:mapping)
        return a:name
    else
        return l:mapping
    endif
endfunction

exec 'nnoremap p :<C-U>call ArgPasteModifyRegister(0)<CR>'.MapArgDefault('p','n')
exec 'nnoremap P :<C-U>call ArgPasteModifyRegister(1)<CR>'.MapArgDefault('P','n')
