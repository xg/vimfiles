if !1 | finish | endif " vim-tiny exits early

set nocompatible

" Vundle
filetype off " Vundle requirement
set runtimepath+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'

Plugin 'tpope/vim-fugitive'
Plugin 'tpope/vim-vinegar'
Plugin 'pangloss/vim-javascript'

if has('python') && (v:version > 703 || (v:version == 703 && has('patch584')))
    Plugin 'Valloric/YouCompleteMe'

    nmap <F10> :YcmCompleter GoTo<CR>
    let g:ycm_extra_conf_globlist = [ '~/dev/*' ]

endif

call vundle#end()

set encoding=utf-8
setglobal fileencoding=utf-8
set fileencodings=ucs-bom,utf-8,latin1
set fileformats=unix,dos
set guifont=SimSun-ExtB:h12:cANSI " 你好

hi ExtraWhitespace ctermbg=grey
match ExtraWhitespace /\s\+$/

set hlsearch
set incsearch

set diffopt+=iwhite
set grepprg=grep\ -nrI\ --exclude-dir=.git\ --exclude=tags\ $*\ /dev/null

" Set title per buffer, with screen support
autocmd BufEnter * let &titlestring = "vim (" . expand("%:t") . ")"
if &term == "screen"
    set t_ts=k
    set t_fs=\
endif
if &term == "screen" || &term == "xterm"
    set title
endif

set wildmenu
set wildignore+=*.o,*.d,*.pyc,*.lo,*.la

set notimeout ttimeout ttimeoutlen=200
set lazyredraw
set scrolloff=5
if version >= 703
    set number
    set relativenumber
    set numberwidth=2
endif
set history=10000

set mouse=a
set ttymouse=xterm2

set backup
set backupdir=$HOME/.vim_backups
set directory=$HOME/.vim_backups/.swaps

if !isdirectory(&backupdir)
    call mkdir(&backupdir)
endif
if !isdirectory(&directory)
    call mkdir(&directory)
endif

set foldmethod=indent
set foldlevel=999

set backspace=indent,eol,start
set autoindent
set smartindent
set ignorecase
set smartcase
set shiftwidth=4
set tabstop=4
set expandtab
set hidden
filetype plugin indent on
syntax on
set guioptions-=gmT

set autowrite
set autowriteall

set cinoptions=(0,:0,W4,g0

" Fix Python indentation to do the same as C.
function! GetIndent(lnum)
    let l:str = matchstr(getline(a:lnum), "^[\t ]*")
    return strlen(l:str)
endfunction

let g:pyindent_open_paren = 'cindent(v:lnum) - GetIndent(v:lnum-1)'

set cmdheight=2
set laststatus=2
set statusline=\ [%n%M%R%W%Y]\ %-50f\ %l/%L\ %P

imap  
command W w
nnoremap <C-L> :nohls<CR><C-L>

" use ghc functionality for haskell files
au BufEnter *.hs compiler ghc
let g:haddock_browser = "C:/Program Files/Mozilla Firefox/firefox.exe"
let g:hs_highlight_boolean = 1
let g:hs_highlight_types = 1
let g:hs_highlight_more_types = 1

au BufEnter *.fx set filetype=cg
au BufEnter *.fxh set filetype=cg
au BufEnter *.cg set filetype=cg
au BufEnter *.as set filetype=actionscript
au BufEnter *.ms set filetype=maxscript
au BufEnter *.mel set filetype=mel
au BufEnter *.ll set filetype=llvm

" au GUIEnter * simalt ~x

nmap <F4> :cnext<CR>
nmap <S-F4> :cprevious<CR>
nmap <F7> :make<CR>

inoremap <BS> <c-g>u<BS>
inoremap <CR> <c-g>u<CR>
inoremap <del> <c-g>u<del>
inoremap <c-w> <c-g>u<c-w>

imap <C-Enter> <c-g>uO
imap <C-S-Enter> <c-g>uo
