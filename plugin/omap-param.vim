"=============================================================================
" $Id$
" File:     plugin/omap-param.vim                                     {{{1
" Maintainer:   Luc Hermitte <EMAIL:hermitte {at} free {dot} fr>
"       <URL:http://code.google.com/p/lh-vim/>
" Other Contributors:   A.Politz
" Version:  1.1.0
" Created:  03rd Sep 2007
" Last Update:  $Date$ (05th Sep 2007)
"------------------------------------------------------------------------
" Description:  
"   Mappings for selecting functions parameters in various programming
"   langages where parameters are passed within braces, separated by
"   commas.
"
" See:
"   :h objet-select
" 
"------------------------------------------------------------------------
" Installation: 
"   Drop this file into {rtp}/plugin/
"   Requires 
"   - Vim 7+,
"   - {rtp}/autoload/lh/position.vim
"   - {rtp}/autoload/lh/syntax.vim
" History:  
"   v0.9.0 first version
" Credits:
"   <URL:http://vim.wikia.com/wiki/Indent_text_object>
"   A.Politz
" TODO:     
"   Move this into lh-vim-lib or a plugin.
" Notes:
"   * "i," can't be used to select several parameters with several uses of
"   "i," ; use "a," instead (-> "va,a,a,"). This is because of simple
"   letter parameters.
"   However, "v2i," works perfectly.
"   * The following should be resistant to &magic, and other mappings
"   * select-mode is not parasited by this plugin
" }}}1
"=============================================================================

if 0
  finish
  call Un(Null,fun2(fun3(a,b,g(NULL))),t, titi, r  , zzz
  call Un(Null,fun2(fun3(a,b,g(NULL))),t, titi,   , zzz)
endif

"=============================================================================
" Avoid global reinclusion {{{1
let s:cpo_save=&cpo
set cpo&vim
if exists("g:loaded_omap_param_vim") 
      \ && !exists('g:force_reload_omap_param_vim')
  let &cpo=s:cpo_save
  finish 
endif
let g:loaded_omap_param_vim = 1
" Avoid global reinclusion }}}1
"------------------------------------------------------------------------
" Public Mappings {{{1
onoremap <silent> i, :<c-u>call <sid>SelectParam(1,0)<cr>
xnoremap <silent> i, :<c-u>call <sid>SelectParam(1,1)<cr><esc>gv
onoremap <silent> a, :<c-u>call <sid>SelectParam(0,0)<cr>
xnoremap <silent> a, :<c-u>call <sid>SelectParam(0,1)<cr><esc>gv

" Public Mappings }}}1

" Private Functions {{{1

function! s:SkipSearch(pattern, flags)
    let l:skip = 1
    while l:skip
        call search(a:pattern, a:flags)
        let l:skip = lh#syntax#skip()

        " at-cursor matching needs special logic to skip
        if l:skip && a:flags =~? 'c'
            if a:flags =~? 'b'
                call search('.', 'b')
            else
                call search('.')
            endif
        endif
    endwhile
endfunction

let g:selectParamDebug = 0

function! s:SelectParam(inner, visual)

    call s:SkipSearch(',\|(\|\[', 'bc') " find start of argument (outer)

    let l:isFirstParam = lh#position#char_at_mark('.') =~? '(\|\['

    if a:inner || l:isFirstParam
        call search('\S') " first non-ws char after separator
    endif

    " this is the start of the parameter, start selection
    normal! v
    call search('.') " don't test the opening character

    let l:depth = []

    let l:count = max([v:count, 1])
    if g:selectParamDebug
        call append(line('$'), l:count)
    endif
    while l:count > 0
        let l:char = lh#position#char_at_mark('.')
        let l:skip = 1

        if l:char =~? '(\|\['
            if g:selectParamDebug
                call append(line('$'), printf('depth+: %s', l:char))
            endif
            let l:depth += [ l:char ]
        elseif l:char =~? ')\|]' && len(l:depth) > 0
            if g:selectParamDebug
                call append(line('$'), printf('depth-: %s', l:char))
            endif
            let l:depth = l:depth[0:-2]
        else
            let l:skip = 0
        endif

        if g:selectParamDebug
            call append(line('$'), printf('testing %s depth %d skip %d', l:char, len(l:depth), l:skip))
        endif
        if l:skip == 0 && l:char =~? ',\|)\|]' && len(l:depth) == 0
            if g:selectParamDebug
                call append(line('$'), 'exiting')
            endif
            if l:char == ','
                let l:count -= 1 " next argumnet
            else
                let l:count = 0 " end of the argument list, end
            endif
        endif

        if l:count > 0
            call s:SkipSearch('(\|,\|)\|\[\|]', '')
        endif
    endwhile

    if a:inner || lh#position#char_at_mark('.') =~? ')\|]' || l:isFirstParam == 0
        call search('\S', 'b') " don't include closing parenthesis
    else
        call search('\S') " match until just before next arg
        call search('.', 'b')
    endif

endfunction

" Private Functions }}}1
"------------------------------------------------------------------------
let &cpo=s:cpo_save
"=============================================================================
" vim600: set fdm=marker:
